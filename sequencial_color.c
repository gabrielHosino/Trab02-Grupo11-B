#include <opencv/highgui.h>
#include <opencv/cxcore.h>
#include <opencv/cv.h>
#include <stdio.h>
#include <stdlib.h>

//gcc sequencial_color.c -o teste `pkg-config --libs --cflags opencv` -ldl
// ./teste img

int main(int argc, char** argv )
{

    int i, j, a, b,c,d;
    int parametersOut[2];
    double somatotal[3], numPixels = 0;
    CvMat *imageIn, *imageOut;
    CvSize size;
    CvScalar rgb;
    imageIn = cvLoadImageM(argv[1], CV_LOAD_IMAGE_COLOR);   //Abre o arquivo com a imagem
    struct timeval tstart, tend;
    int tmili, min, segs;


    //Se não conseguir abrir a imagem retorna erro
    if (!imageIn)
    {
        printf("No image data \n");
        return -1;
    }

    //prepara o vertor auxiliar de soma dos pixels
    for(a = 0; a < 3; a++){
        somatotal[a] = 0;
    }  
    
    gettimeofday(&tstart, NULL);
    //comeca a percorrer a matriz de pixels da imagem
    for(i = 0; i < imageIn->rows; i++){
        for(j= 0; j < imageIn->cols; j++){
            //variaveis auxiliares de comparacao
            c = i - 2;
            d = j - 2;
            for(a = i - 2;a < c+5; a++){
                for(b = j - 2; b < d+5; b++){
                    if( a >= 0 && b >= 0 && a < imageIn->rows && b < imageIn->cols){
                        //pega o pixel e faz a soma de cada componente do RGB
                        rgb = cvGet2D(imageIn, a, b);
                        somatotal[0] = somatotal[0] + rgb.val[0];
                        somatotal[1] = somatotal[1] + rgb.val[1];
                        somatotal[2] = somatotal[2] + rgb.val[2];
                        numPixels++;
                    }
                        
                }
            }   
            //passa o resultado final para a variavel rgb 
            for(a = 0; a < 3; a++){
                rgb.val[a] = somatotal[a]/numPixels;
            } 
            //seta os novos valores na imagem de saida  
            cvSet2D(imageIn, i, j,rgb);
            //retorna os valores das variaveis auxliares
            numPixels = 0;
            for(a = 0; a < 3; a++){
                somatotal[a] = 0;
             }  
        }
    }
    gettimeofday(&tend, NULL); 
    tmili = (int) (1000 * (tend.tv_sec - tstart.tv_sec) + (tend.tv_usec - tstart.tv_usec) / 1000);

    printf("Tempo: %d\n milisegs", tmili);
    //Seta os parametros que serao usados na hora de salvar a nova imagem em disco
    parametersOut[1] = CV_IMWRITE_PNG_COMPRESSION;
    parametersOut[2] = 3;
    
    //Salva a nova imagem em disco
    cvSaveImage("out.jpg", imageIn, parametersOut);

    cvReleaseMat(&imageIn);

    return 0;
}

