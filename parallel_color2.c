#include <opencv/highgui.h>
#include <opencv/cxcore.h>
#include <opencv/cv.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <omp.h>
#include <mpi.h>

//mpicc parallel_color2.c -o teste -fopenmp `pkg-config --libs --cflags opencv` -ldl

void timeval_print(struct timeval end,struct timeval start){
    int tmili, min, segs;

    tmili = (int) (1000 * (end.tv_sec - start.tv_sec) + (end.tv_usec - start.tv_usec) / 1000);

    printf("Tempo: %d\n milisegs", tmili);
}


int *createMatrix (int nrows, int ncols) {
    int *matrix;

    if (( matrix = malloc(nrows*ncols*sizeof(int))) == NULL) {
        printf("Malloc error");
        exit(1);
    }

    return matrix;
}


int main(int argc, char** argv )
{

    int i, j, a, b,c,d;
    int parametersOut[2];
    int somatotal[3], numPixels = 0;
    int *imageToVecR, *imageToVecG, *imageToVecB;
    CvMat *imageIn;
    CvScalar rgb;
    int lines, cols;
    struct timeval tstart, tend;
    

    //prepara o vertor auxiliar de soma dos pixels
    for(a = 0; a < 3; a++){
        somatotal[a] = 0;
    }  
    
    MPI_Init(NULL,NULL);

    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    //Aqui tem que criar 2 novos grupos alem do inicial
    //cada grupo ficara encarregado de computar uma cor do RGB
    //depois voltam os resultados

    if(world_rank == 0){
        imageIn = cvLoadImageM( argv[1], CV_LOAD_IMAGE_COLOR);   //Abre o arquivo com a imagem
    
        //Se não conseguir abrir a imagem retorna erro
        if (!imageIn)
        {
            printf("No image data \n");
            return -1;
        }

        imageToVecR = createMatrix(imageIn->rows,imageIn->cols);
        imageToVecG = createMatrix(imageIn->rows,imageIn->cols);
        imageToVecB = createMatrix(imageIn->rows,imageIn->cols);


        for(i = 0; i < imageIn->rows; i++){
            for(j = 0; j < imageIn->cols; j++){
                rgb = cvGet2D(imageIn, i, j);
                imageToVecB[i*imageIn->cols + j] = (int)rgb.val[0];
                imageToVecG[i*imageIn->cols + j] = (int)rgb.val[1];
                imageToVecR[i*imageIn->cols + j] = (int)rgb.val[2];

            }   
        }
        
        lines = imageIn->rows / world_size; //Vai mudar para group size
        cols = imageIn->cols;
    }

    if(world_rank == 0){
        gettimeofday(&tstart, NULL);
    }

    MPI_Bcast(&lines, 1, MPI_INT, 0 ,MPI_COMM_WORLD);
    MPI_Bcast(&cols, 1, MPI_INT, 0 ,MPI_COMM_WORLD);
   

    int *auxR = createMatrix(lines,cols);
    int *auxG = createMatrix(lines,cols); 
    int *auxB = createMatrix(lines,cols);  

    MPI_Scatter(imageToVecR, lines*cols, MPI_INT, auxR, lines*cols, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Scatter(imageToVecG, lines*cols, MPI_INT, auxG, lines*cols, MPI_INT, 0, MPI_COMM_WORLD); 
    MPI_Scatter(imageToVecB, lines*cols, MPI_INT, auxB, lines*cols, MPI_INT, 0, MPI_COMM_WORLD);


    MPI_Barrier(MPI_COMM_WORLD);    

  
    omp_set_dynamic(0);     
    omp_set_num_threads(8);

    #pragma omp parallel 
    

    #pragma omp for private(i,j,a,b,c,d) firstprivate(somatotal, rgb, numPixels)
    //comeca a percorrer a matriz de pixels da imagem
    for(i = 0; i < lines; i++){
        for(j= 0; j < cols; j++){
            //variaveis auxiliares de comparacao
            c = i - 2;
            d = j - 2;
            for(a = i - 2;a < c+5; a++){
                for(b = j - 2; b < d+5; b++){
                    if( a >= 0 && b >= 0 && a < lines && b < cols){
                        
                        somatotal[0] = somatotal[0] + auxB[a*cols + b];
                        somatotal[1] = somatotal[1] + auxG[a*cols + b];
                        somatotal[2] = somatotal[2] + auxR[a*cols + b];
                        numPixels++;
                    }
                        
                }
            }
            auxB[i*cols +j] = somatotal[0]/numPixels;
            auxG[i*cols +j] = somatotal[1]/numPixels;
            auxR[i*cols +j] = somatotal[2]/numPixels;
         
            //retorna os valores das variaveis auxliares
            numPixels = 0;
            for(a = 0; a < 3; a++){
                somatotal[a] = 0;
            }

        }
    }
    
    MPI_Barrier(MPI_COMM_WORLD);

    //retorna o resultado de cada processo para o processo pai
    MPI_Gather(auxR, lines*cols, MPI_INT, imageToVecR, lines*cols, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Gather(auxG, lines*cols, MPI_INT, imageToVecG, lines*cols, MPI_INT, 0, MPI_COMM_WORLD); 
    MPI_Gather(auxB, lines*cols, MPI_INT, imageToVecB, lines*cols, MPI_INT, 0, MPI_COMM_WORLD);

    MPI_Barrier(MPI_COMM_WORLD);

    free(auxR);
    free(auxB);
    free(auxG);

    if(world_rank == 0){
        if(world_size*lines < imageIn->rows){
            for(i = world_size*lines; i < imageIn->rows; i++){
                for(j= 0; j < imageIn->cols; j++){
                    //variaveis auxiliares de comparacao
                    c = i - 2;
                    d = j - 2;
                    for(a = i - 2;a < c+5; a++){
                        for(b = j - 2; b < d+5; b++){
                            if( a >= 0 && b >= 0 && a < imageIn->rows && b < imageIn->cols){
                                
                                somatotal[0] = somatotal[0] + imageToVecB[a*imageIn->cols + b];
                                somatotal[1] = somatotal[1] + imageToVecG[a*imageIn->cols + b];
                                somatotal[2] = somatotal[2] + imageToVecR[a*imageIn->cols + b];
                                numPixels++;
                            }
                                
                        }
                    }
                    imageToVecB[i*cols +j] = somatotal[0]/numPixels;
                    imageToVecG[i*cols +j] = somatotal[1]/numPixels;
                    imageToVecR[i*cols +j] = somatotal[2]/numPixels;
                 
                    //retorna os valores das variaveis auxliares
                    numPixels = 0;
                    for(a = 0; a < 3; a++){
                        somatotal[a] = 0;
                    }
                }
            }
        }
        gettimeofday(&tend, NULL); 
        //volta para o padrao do opencv para poder criar a nova imagem
        for(i = 0; i < imageIn->rows; i ++){
            for(j = 0; j < imageIn->cols; j++){
                rgb.val[0] = (double)imageToVecB[i*imageIn->cols +j];
                rgb.val[1] = (double)imageToVecG[i*imageIn->cols +j];
                rgb.val[2] = (double)imageToVecR[i*imageIn->cols +j];
                    
                cvSet2D(imageIn, i, j,rgb); 
            }
        } 
        free(imageToVecR);
        free(imageToVecG);
        free(imageToVecB);    
        //Seta os parametros que serao usados na hora de salvar a nova imagem em disco
        parametersOut[1] = CV_IMWRITE_PNG_COMPRESSION;
        parametersOut[2] = 3;
        
        //Salva a nova imagem em disco
        cvSaveImage("out2.jpg", imageIn, parametersOut);

       

        timeval_print(tend, tstart);
    }

    MPI_Finalize();
    return 0;
}

