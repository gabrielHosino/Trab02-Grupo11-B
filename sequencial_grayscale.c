#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>

#include <opencv/highgui.h>
#include <opencv/cxcore.h>
#include <opencv/cv.h>

//gcc sequencial_grayscale.c -o teste `pkg-config --libs --cflags opencv` -ldl
//./teste imagem

int main(int argc, char** argv )
{

    int i, j, a, b, c ,d;
    int parametersOut[2];
    int pixel, somatotal = 0, numPixels = 0;
    CvMat *imageIn, *imageOut;
    CvSize size;
    imageIn = cvLoadImageM( argv[1], CV_LOAD_IMAGE_GRAYSCALE);   //Abre o arquivo com a imagem
    imageOut = cvCloneMat(imageIn);                              //Copia a imagem de entrada
    struct timeval tstart, tend;
    int tmili, min, segs;


    //Se não conseguir abrir a imagem retorna erro
    if (!imageIn)
    {
        printf("No image data \n");
        return -1;
    }
    
    gettimeofday(&tstart, NULL);
    //comeca a percorrer a matriz de pixels da imagem    
    for(i = 0; i < imageIn->rows; i++){
        for(j= 0; j < imageIn->cols; j++){
            //variaveis auxiliares de comparacao
            c = i - 2;
            d = j - 2;
            for(a = i - 2;a < c+5; a++){
                for(b = j - 2; b < d+5; b++){
                    if( a >= 0 && b >= 0 && a < imageIn->rows && b < imageIn->cols){
                        //pega o valor do pixel e soma 
                        pixel = CV_MAT_ELEM(*imageIn, unsigned char, a,b);
                        somatotal = somatotal + pixel;
                        numPixels++;
                    }
                        
                }
            }
            //salva o novo valor na imagem de saida    
            CV_MAT_ELEM(*imageOut, unsigned char, i, j) = somatotal/numPixels;   
            somatotal = 0;
            numPixels = 0;
        }
    }
    gettimeofday(&tend, NULL); 
    tmili = (int) (1000 * (tend.tv_sec - tstart.tv_sec) + (tend.tv_usec - tstart.tv_usec) / 1000);

    printf("Tempo: %d\n milisegs", tmili);
    //Seta os parametros que serao usados na hora de salvar a nova imagem em disco
    parametersOut[1] = CV_IMWRITE_PNG_COMPRESSION;
    parametersOut[2] = 3;
    
    //Salva a nova imagem em disco
    cvSaveImage("out.jpg", imageOut, parametersOut);

    cvReleaseMat(&imageIn);
    cvReleaseMat(&imageOut);

    return 0;
}

