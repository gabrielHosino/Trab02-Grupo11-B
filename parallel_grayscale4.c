#include <opencv/highgui.h>
#include <opencv/cxcore.h>
#include <opencv/cv.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <omp.h>
#include <mpi.h>

//mpicc parallel_grayscale3.c -o teste -fopenmp `pkg-config --libs --cflags opencv` -ldl
//mpirun -np numprocessos ./teste imagem

int *createMatrix (int nrows, int ncols) {
    int *matrix;

    if (( matrix = malloc(nrows*ncols*sizeof(int))) == NULL) {
        printf("Malloc error");
        exit(1);
    }

    return matrix;
}

void timeval_print(struct timeval end,struct timeval start){
    int tmili, min, segs;

    tmili = (int) (1000 * (end.tv_sec - start.tv_sec) + (end.tv_usec - start.tv_usec) / 1000);

    printf("Tempo: %d\n milisegs", tmili);
}

int main(int argc, char** argv )
{

    int i, j, a, b, c ,d;
    int parametersOut[2];
    int pixel, somatotal = 0, numPixels = 0;
    int *imageToVec;
    CvMat *imageIn; 
    struct timeval tstart, tend;
    int lines, cols;
       

   

    MPI_Init(&argc,&argv);

    int world_rank;
    MPI_Comm_rank(MPI_COMM_WORLD, &world_rank);
    int world_size;
    MPI_Comm_size(MPI_COMM_WORLD, &world_size);

    //No processo pai abre a imagem cria um vetor aux com a imagem 
    //esse vetor vai depois ser dividido entre os processos
    if(world_rank == 0){
        
        imageIn = cvLoadImageM( argv[1], CV_LOAD_IMAGE_GRAYSCALE);   //Abre o arquivo com a imagem
        //Se não conseguir abrir a imagem retorna erro
        if (!imageIn)
        {
            printf("No image data \n");
            return -1;
        }  

        imageToVec = createMatrix(imageIn->rows,imageIn->cols);

        for(i = 0; i < imageIn->rows; i++){
            for(j = 0; j < imageIn->cols; j++){
                imageToVec[i*imageIn->cols + j] = CV_MAT_ELEM(*imageIn, unsigned char, i, j);

            }   
        }
        //variaveis que auxiliam os processos saberem o tamanho do 
        //trabalho que cada um tem
        lines = imageIn->rows / world_size;
        cols = imageIn->cols;
    }

    if(world_rank == 0){
        gettimeofday(&tstart, NULL);
    }


    MPI_Bcast(&lines, 1, MPI_INT, 0 ,MPI_COMM_WORLD);
    MPI_Bcast(&cols, 1, MPI_INT, 0 ,MPI_COMM_WORLD);
    //MPI_Bcast(imageToVec, 1 , MPI_INT, 0, MPI_COMM_WORLD); 

    //printf("Ponteiro: %p POCESSO: %d\n", &imageToVec, world_rank);
    //printf("POCESSO: %d Valor: %d\n", world_rank, imageToVec[10]);


    int *aux = createMatrix(lines,cols); 
	
    
    //divide a imagem entre os processos
    MPI_Scatter(imageToVec, lines*cols, MPI_INT, aux, lines*cols, MPI_INT, 0, MPI_COMM_WORLD);   

    omp_set_dynamic(0);     
    omp_set_num_threads(8);
    #pragma omp parallel 


    #pragma omp for private(i,j,a,b,c,d) firstprivate(somatotal, pixel, numPixels)
    
    for(i = 0; i < lines; i++){
        
        for(j= 0; j < cols; j++){
            //variaveis auxiliares de comparacao
            c = i - 2;
            d = j - 2;

            for(a = i - 2;a < c+5; a++){
                for(b = j - 2; b < d+5; b++){
                    
                    if( a >= 0 && b >= 0 && a < lines && b < cols){
                        //pega o valor do pixel e soma 
                        pixel = aux[a*cols + b];
                        somatotal = somatotal + pixel;
                        numPixels++;
                    }
                        
                }
            }

            //salva o novo valor na auxiliar 
            aux[i*cols + j] = somatotal/numPixels;     
            somatotal = 0;
            numPixels = 0;
         
        }
    }

    
  
    //retorna o resultado de cada processo para o processo pai
    MPI_Gather(aux, lines*cols, MPI_INT, imageToVec, lines*cols, MPI_INT, 0, MPI_COMM_WORLD);
   

    MPI_Barrier(MPI_COMM_WORLD);
    //Volta para o processo pai
    //Ele checa para ver se toda a imagem pode ser processada 
    //se não ele processa a parte final
    if(world_rank == 0){
        if(world_size*lines < imageIn->rows){
           for(i = world_size*lines; i < imageIn->rows; i++){
        
                for(j= 0; j < imageIn->cols; j++){
                //variaveis auxiliares de comparacao
                c = i - 2;
                d = j - 2;

                    for(a = i - 2;a < c+5; a++){
                        for(b = j - 2; b < d+5; b++){
                        
                            if( a >= 0 && b >= 0 && a < imageIn->rows && b < imageIn->cols){
                                //pega o valor do pixel e soma 
                                pixel = imageToVec[a*imageIn->cols + b];
                                somatotal = somatotal + pixel;
                                numPixels++;
                            }
                            
                        }
                    }

                    //salva o novo valor na auxiliar 
                    imageToVec[i*imageIn->cols + j] = somatotal/numPixels;     
                    somatotal = 0;
                    numPixels = 0;
         
                }
            } 
        }

        gettimeofday(&tend, NULL); 

        //volta para o padrao do opencv para poder criar a nova imagem
        for(i = 0; i < imageIn->rows; i ++){
            for(j = 0; j < imageIn->cols; j++){
                CV_MAT_ELEM(*imageIn, unsigned char, i, j) = imageToVec[i*imageIn->cols + j];  
            }
        }
        
        //libera o vetor com a imagem   
        free(imageToVec);
        

        //Seta os parametros que serao usados na hora de salvar a nova imagem em disco
        parametersOut[1] = CV_IMWRITE_PNG_COMPRESSION;
        parametersOut[2] = 3;
        
        //Salva a nova imagem em disco
        cvSaveImage("out2.jpg", imageIn, parametersOut);

        //libera a imagem lida pelo opencv 
        cvReleaseMat(&imageIn);

        timeval_print(tend, tstart);

    }

    //libera a memoria alocada
    free(aux);
    
   

   MPI_Finalize(); 


    return 0;
}

